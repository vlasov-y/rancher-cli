#!/usr/bin/env sh

info() {
	GREEN='\033[0;33m'
	NC='\033[0m' # No Color
	printf "${GREEN}[INFO] $@ ${NC}\n"
}

error() {
	RED='\033[0;31m'
	NC='\033[0m' # No Color
	printf "${RED}[ERROR] $@ ${NC}\n"
}

print_help_message() {
	info '''HELP MESSAGE
You can use container in to different ways:
1. Control performed action setting up variables ACTION, STACK, SERVICE, CONTAINER (automated mode)
	Actions:
		upgrade - upgrade service forcing rancher to pull latest image
		confirm/rollback - confirm or rollback upgrade
		wait-stopped - wait until container stop
	Also, you can specify IMAGE variable and rancher will set new docker image for service before upgrade
2. Call rancher binary directly with all needed args (manual mode)

Automated mode examples:
---
	docker run --rm
		-v /path/to/credentials.env:/run/secrets/env
		-e ACTION=upgrade
		-e IMAGE=my_account/new_soft:v1.2.3
		-e STACK=MyApp-Development
		-e SERVICE=my-app-backend
		yuriyvlasov/rancher-cli
---
	docker run --rm
		-e RANCHER_URL=https:\\my-rancher.server.com
		-e RANCER_ACCESS_KEY=longalphanum
		-e RANCER_SECRET_KEY=longalphanum
		-e RANCER_ENVIRONMENT=1a10
		-e ACTION=rollback
		-e STACK=MyApp-Development
		-e SERVICE=my-app-backend
		yuriyvlasov/rancher-cli
---
	docker run --rm
		-v /path/to/credentials.env:/run/secrets/env
		-e ACTION=confirm
		-e STACK=MyApp-Development
		-e SERVICE=my-app-backend
		yuriyvlasov/rancher-cli
---
	docker run --rm
		-v /path/to/credentials.env:/run/secrets/env
		-e ACTION=wait-stopped
		-e CONTAINER=MyAPP-Production-db-backup-1
		yuriyvlasov/rancher-cli
---

Manual mode example:
	docker run --rm 
		-v /path/to/credentials.env:/run/secrets/env
		yuriyvlasov/rancher-cli
		rancher 
			-d 
			--rancher-file StackDev/rancher-compose.yml 
			--file StackDev/docker-compose.yml 
			--confirm-upgrade 
			--stack StackDev my-service
'''
}

rancher_wait_container_stopped() {
	WAIT_TIME_LIMIT=120
	CHECK_INTERVAL=5

	WAIT_TIME_LEFT=$WAIT_TIME_LIMIT
	while [ $WAIT_TIME_LEFT -gt 0 ] && rancher inspect --type container $CONTAINER | jq -r .state | egrep -v stopped; do
		sleep $CHECK_INTERVAL;
		WAIT_TIME_LEFT=$(($WAIT_TIME_LEFT - $CHECK_INTERVAL))
	done
	if [ $WAIT_TIME_LEFT -le 0 ]; then
		echo "[ERROR] Failed waiting container stop by timeout in $WAIT_TIME_LEFT seconds." 1>&2
		return 1
	fi
	info "Container stopped. Time elapsed: $(($WAIT_TIME_LIMIT - $WAIT_TIME_LEFT)) seconds."
}

rancher_confirm() {
	rancher --env "$RANCHER_ENVIRONMENT" up -d \
		--rancher-file "$STACK/rancher-compose.yml" \
		--file "$STACK/docker-compose.yml" \
		--confirm-upgrade \
		--stack "$STACK" \
		"$SERVICE"
}

rancher_upgrade() {
	rancher_confirm
	if [ "$IMAGE" != "" ]; then
		# set new docker image
		yq w "$STACK/docker-compose.yml" "services.${SERVICE}.image" "$IMAGE"
	fi
	rancher --env "$RANCHER_ENVIRONMENT" up -d \
		--rancher-file "$STACK/rancher-compose.yml" \
		--file "$STACK/docker-compose.yml" \
		--pull \
		--upgrade \
		--force-upgrade \
		--stack "$STACK" \
		"$SERVICE"
}

rancher_rollback() {
	rancher --env "$RANCHER_ENVIRONMENT" up -d \
		--rancher-file "$STACK/rancher-compose.yml" \
		--file "$STACK/docker-compose.yml" \
		--rollback \
		--stack "$STACK" \
		"$SERVICE"
}