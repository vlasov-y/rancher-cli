#!/usr/bin/env sh

set -e

reset_passwords() {
	sudo /bin/sh `pwd`/random-passwords.sh
}

generate_rancher_config() {
	CONFIG_FILE="${CONFIG_FILE:-/run/secrets/env}"
	if [ -f "$CONFIG_FILE" ]; then
		. "$CONFIG_FILE"
	fi
	# login
	info "Creating cli.json for login to Rancher"
	mkdir -p ~/.rancher
	cat <<EOF >~/.rancher/cli.json
{
    "accessKey": "${RANCHER_ACCESS_KEY}",
    "environment": "${RANCHER_ENVIRONMENT}",
    "secretKey": "${RANCHER_SECRET_KEY}",
    "url": "${RANCHER_URL%%/}/v2-beta/schemas"
}
EOF
	# export current config
	info "Export of current configuration"
	rancher --env "$RANCHER_ENVIRONMENT" export
	# adding start_before_stop setting to all the services
	if [ "${START_BEFORE_STOP:-true}" = 'true' ]; then
		for RC in `find . -name rancher-compose.yml`; do
			for S in `yq r "$RC" services | egrep -o '^[^ :]+'`; do
				yq w -i "$RC" "services.${S}.upgrade_strategy.start_first" true;
			done
		done
	fi
	# just for info
	find . -name rancher-compose.yml
}

. ./rancher-functions.sh
reset_passwords
generate_rancher_config

if [ $# -gt 0 ]; then
	exec "$@"
else
	if [ "$ACTION" != "" ]; then
		# if used short version of control
		# export control variables
		export ACTION STACK SERVICE IMAGE RANCHER_ENVIRONMENT
		# call appropriate function
		case $ACTION in
			confirm)
				info "Confirm upgrade of ${SERVICE} in ${STACK}"
				rancher_confirm
			;;
			upgrade)
				info "Upgrade ${SERVICE} in ${STACK}"
				rancher_upgrade
			;;
			rollback)
				info "Rollback upgrade of ${SERVICE} in ${STACK}"
				rancher_rollback
			;;
			wait-stopped)
				info "Wait until container ${CONTAINER} stop"
				rancher_wait_container_stopped
			;;
		esac
	else
		print_help_message
	fi
fi
