## Rancher CLI for automated CI/CD  
Rancher CLI exports configuration from working cluster and trigger update of asked service.  
Rancher CLI can work in two modes: **Manual** and **Automated**

In manual mode you can call rancher cli manually (only if you are experienced in rancher-cli):
```bash
docker run --rm \
	-v /path/to/credentials.env:/run/secrets/env \
	yuriyvlasov/rancher-cli \
	rancher -d \
		--rancher-file StackDev/rancher-compose.yml \
		--file StackDev/docker-compose.yml \
		--confirm-upgrade \
		--stack StackDev my-service\
```
Or do the same thing in automated (preffered):
```bash
docker run --rm \
	-v /path/to/credentials.env:/run/secrets/env \
	-e ACTION=confirm \
	-e STACK=StackDev \
	-e SERVICE=my-service \
	yuriyvlasov/rancher-cli
```

### Environment variables  
| NAME | Mode | DEFAULT | DESCRIPTION |  
| ---  | ---      | ---     | ---         |  
| CONFIG_FILE			| all | /run/secrets/env | File with environment variables |  
| RANCHER_URL 			| all | | URL of rancher server |  
| RANCHER_ACCESS_KEY	| all | | API access key |  
| RANCHER_SECRET_KEY	| all | | API secret key |  
| RANCHER_ENVIRONMENT	| all | | Rancher environment ID (as example: 1a10) |  
| START_BEFORE_STOP		| auto | true | If 'true', then service will start new container before shutting of old one |  
| ACTION				| auto | | Action to perform: confirm, upgrade, rollback, wait-stopped |  
| STACK					| auto | | Rancher stack |  
| SERVICE 				| auto | | Service in stack |  
| CONTAINER 			| auto | | Container to wait (only for action wait-stopped) |  
| IMAGE					| auto | | New image to deploy (only for upgrade) |  

### Examples  
You should use either **CONFIG_FILE** file, or set of **RANCHER_\*** vars.  

#### CONFIG_FILE  
Lets assume that we created file **env**:  
```bash  
export RANCHER_URL="https://my-rancher.com"  
export RANCHER_ACCESS_KEY="access"  
export RANCHER_SECRET_KEY="secret"  
export RANCHER_ENVIRONMENT="1a10"
```  
So that we can run container like this  
```bash  
docker run -e CONFIG_FILE=/run/secrets/env -v `pwd`:/run/secrets rancher-cli rancher --help  
```  
Or like this  
```bash  
docker run -v `pwd`/env:/app/env rancher-cli rancher --help  
```  
Also, we can use **START_BEFORE_STOP**  
```bash  
docker run -e START_BEFORE_STOP=true -v `pwd`/env:/app/env rancher-cli rancher --help  
```  
