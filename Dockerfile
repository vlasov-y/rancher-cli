FROM alpine
WORKDIR /app
ENTRYPOINT [ "/app/entrypoint.sh" ]

# adding user, installing sudo, setting sudo permissions
RUN apk upgrade --update --no-cache && \
	apk add --update --no-cache sudo ca-certificates jq && \
	adduser -h /app -s /bin/ash -u 2020 -D app && \
	echo 'app ALL=(root) NOPASSWD: /bin/sh /app/random-passwords.sh' > /etc/sudoers.d/app

# download and install rancher-cli and yq
ADD https://github.com/mikefarah/yq/releases/download/2.4.0/yq_linux_amd64 /bin/yq
ADD https://releases.rancher.com/cli/v0.6.13/rancher-linux-amd64-v0.6.13.tar.gz /tmp/rancher-linux-amd64-v0.6.13.tar.gz
RUN	tar -C /tmp -xpzvf /tmp/rancher-linux-amd64-v0.6.13.tar.gz && \
	mv /tmp/rancher-v0.6.13/rancher /bin/rancher && \
	chown root:root /bin/rancher && \
	chmod 0755 /bin/rancher /bin/yq

# copying all scripts
COPY ./app/* ./
RUN chown root:root ./*.sh && \
	chmod 0755 ./*.sh && \
	chmod a+rwxt /tmp

# set app user default
USER app